

initNumber: int = int(input("Introduce un numero inicial: "))
finalNumber: int = int(input("Introduce un numero final: "))
numerosImpares: int = []

while finalNumber <= initNumber:
    finalNumber: int = int(input('El segundo número debe ser mayor que el primero. Inroduce otro número: '))

for i in range(initNumber, finalNumber+1):
    if(i%2!=0):
        numerosImpares.append(i)

print(f"Lista de Números impares entre {initNumber} y {finalNumber}:")

print(numerosImpares)

